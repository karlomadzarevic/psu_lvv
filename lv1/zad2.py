
from pydoc import doc

ocjena = input("Unesite ocjenu izmedu 0.0 i 1.0: ")

try:
    ocjena = float(ocjena)
except:
    print("Niste unijeli broj")
    exit()

while (ocjena < 0.0) or (ocjena > 1.0):
    ocjena = input("Unesite ocjenu izmedu 0.0 i 1.0: ")
    try:
        ocjena = float(ocjena)
    except:
        print("Niste unijeli broj")
        exit()

if ocjena >= 0.9:
    print("A")
elif ocjena >= 0.8:
    print("B")
elif ocjena >= 0.7:
    print("C")
elif ocjena >= 0.6:
    print("D")
elif ocjena < 0.6:
    print("F")   