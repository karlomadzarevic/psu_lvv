import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors
from matplotlib.ticker import PercentFormatter
 
rollResults = dict()
rolls = []
for i in range (101):
    diceSide = np.random.randint(1,7);
    rolls.append(diceSide)
    if diceSide not in rollResults:
        rollResults[diceSide] = 1
    else:
        rollResults[diceSide] +=1

for i in range(1,7):
    print(rollResults.get(i))
    
plt.hist(rolls,bins = 6)
plt.show()