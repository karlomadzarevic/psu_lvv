
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors
from matplotlib.ticker import PercentFormatter

data = np.loadtxt(open("mtcars.csv", "rb"), usecols = (1,2,3,4,5,6), delimiter = ",", skiprows = 1)

mpgValue = []
hpValue = []
wtValue = []
mpgLargest = data [0][0]
mpgLowest = data [0][0]
sum = 0
count = 0
for mpg in data:
    mpgValue.append(mpg[0])
    hpValue.append(mpg[3])
    wtValue.append(mpg[5] * 10)
    if(mpg[1] == 6):
        sum = sum + mpg[0]
        count = count + 1
        if(mpg[0] > mpgLargest):
            mpgLargest = mpg[0]
        if(mpg[0] < mpgLowest):
            mpgLowest = mpg[0]
    

    
print(sum/count)
print(mpgLowest)
print(mpgLargest)
plt.title("Ovisnost potrosnje o hp")
plt.xlabel("milja po galonu")
plt.ylabel("konjska snaga")

plt.scatter(mpgValue,hpValue,wtValue)
