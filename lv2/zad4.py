
import matplotlib.pyplot as plt
import numpy as np
import skimage.io

img = skimage.io.imread('tiger.png', as_gray = True)

maximum = img.max()
minimum = img.min()
print("maks = ", maximum)
print("min = ", minimum)

rows, cols = img.shape

imgIncreasedBrightness = img.copy()

for i in range(rows):
    for j in range(cols):
        imgIncreasedBrightness[i][j] += 45


plt.figure(1)
plt.imshow(img, cmap = 'gray', vmin = 0, vmax = 255)
plt.title("originalna slika")
plt.figure(2)
plt.imshow(imgIncreasedBrightness, cmap = 'gray', vmin = 0, vmax = 255)
plt.show()


rotateImg = np.zeros((cols, rows))
for i in range(rows):
    for j in range(cols):
        rotateImg[j][rows - 1 - i] = img[i][j]

plt.figure(3)
plt.imshow(img, cmap = 'gray', vmin = 0, vmax = 255)
plt.figure(4)
plt.imshow(rotateImg, cmap = 'gray', vmin = 0, vmax = 255)
plt.show()


mirroredImage = np.zeros((rows, cols))
for i in range(rows):
    for j in range(cols):
        mirroredImage[i][j] = img[i][cols - 1 - j]

plt.figure(5)
plt.imshow(img, cmap = 'gray', vmin = 0, vmax = 255)
plt.figure(6)
plt.imshow(mirroredImage, cmap = 'gray', vmin = 0, vmax = 255)
plt.show()       



LoweredResolutionImage = np.zeros(((int(rows/10)), (int(cols/10))))
for i in range(int(rows/10)):
    for j in range(int(cols/10)):
        LoweredResolutionImage[i][j] = img[i*10][j*10]

plt.figure(7)
plt.imshow(img, cmap = 'gray', vmin = 0, vmax = 255)
plt.figure(8)
plt.imshow(LoweredResolutionImage, cmap = 'gray', vmin = 0, vmax = 255)
plt.show()       
  

broj = int(cols/4)
broj2 = int(cols/2)
alteredImage = img.copy()
for i in range(rows):
    for j in range(cols):
        if(j < broj or j > broj2):
            alteredImage[i][j] = 0



plt.figure(9)
plt.imshow(img, cmap = 'gray', vmin = 0, vmax = 255)
plt.figure(10)
plt.imshow(alteredImage, cmap = 'gray', vmin = 0, vmax = 255)
plt.show()  
