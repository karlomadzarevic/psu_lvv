
import matplotlib.pyplot as plt
import numpy as np
import skimage.io

sqSize = 25
brojPolja = 10

black = np.zeros((sqSize, sqSize))
white = 255 * np.ones((sqSize, sqSize))

row1 = black
row2 = white
for i in range(brojPolja - 1):
    print(i)
    if i % 2 == 0:
        row1 = np.hstack((row1, white))
        row2 = np.hstack((row2, black))
    else:
        row1 = np.hstack((row1, black))
        row2 = np.hstack((row2, white))





