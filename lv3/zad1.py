import pandas as pd
import numpy as np


mtcars = pd.read_csv('mtcars.csv')
#1.
##print(mtcars.sort_values(by = ['mpg']).head(5)['car'])

#2.
##print( mtcars.sort_values(by = ['mpg'])[mtcars.cyl == 8].tail(3))

#3.
##print(mtcars['mpg'][mtcars.cyl == 6].mean())

#4.
##print(mtcars [ (mtcars.cyl == 4) & (mtcars.wt > 2.0) & (mtcars.wt < 2.2)].mpg.mean())

#5.
## print(mtcars.groupby('am').count().car[0] )
##print(mtcars.groupby('am').count().car[1] )

#6.
##data = mtcars[(mtcars.am == 0) & (mtcars.hp > 100)]
##print(len(data))

#7.
mtcars['kg'] = mtcars['wt'] * 1000 * 0.453
print(mtcars['car' , 'kg'])


