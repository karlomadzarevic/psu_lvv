import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv')

#1
##new_mtcars = mtcars.groupby('cyl').mpg.mean().plot.bar()
##plt.show()

#2
##boxplot = mtcars.boxplot(by='cyl', column='wt')
##plt.show()

#3
##kpl = mtcars.groupby('am').mpg.mean().plot.bar()
##plt.show()

#4
ax = mtcars[mtcars.am==0].plot.scatter(x='qsec',y='hp',color='Blue',label='0')
mtcars[mtcars.am==1].plot.scatter(x='qsec',y='hp',color='red',label='1',ax=ax)
plt.show()


